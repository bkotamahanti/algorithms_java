package Arrays;

import java.util.Arrays;

/**
 * 
 * @author bkotamahanti
 * https://leetcode.com/problems/maximum-subarray/
 * 
	Given an integer array nums, find the contiguous subarray (containing at least one number) which has the largest sum and return its sum.

	Example:

	Input: [-2,1,-3,4,-1,2,1,-5,4],
	Output: 6
	Explanation: [4,-1,2,1] has the largest sum = 6.
 *
 */
public class MaximumSubArray_LeetCode {
	/**
	 * 
	 * @param nums
	 * @return
	 * Time: O(n**2)
	 * Space:O(1)
	 */
	public static int maxSubArrayBruteForce(int[] nums) {
		if((nums==null) || (nums.length==0)) {
			return 0;
		}
		if(nums.length==1) {
			return nums[0];
		}

		int sum, max_sum=nums[0];
		int n=nums.length;

		for(int i=0;i<n;i++) {
			sum=0;
			for(int j=i;j<n;j++) {
				sum=sum+nums[j];
				max_sum=Math.max(sum, max_sum);
			}
		}
		return max_sum;
	}


	/**
	  [-2,1,-3,4,-1,2,1,-5,4]
	  [1,2,3,4,5,6]
	  [-2 -3 -5 -8 -1 -6]
	  []
	  [-1]
	  [1]
	 **
	 *Greedy approach
	 *Time: O(n)
	 *Space:O(1)
	 */

	public static int maxSubArray(int[] nums) {
		if (nums==null ){
			return 0;
		}else if(nums.length==0){
			return 0;
		}else if(nums.length==1){
			return nums[0];
		}else{
			int sum=0;
			int max_sum=nums[0];
			int i=0;
			int n=nums.length;
			while(i<n){
				sum=sum+nums[i];
				max_sum=Math.max(sum,max_sum);
				if(sum<0){
					sum=0;
				}
				i++;
			}
			return max_sum;
		}
	}


	public static void main(String[] args) {
		/*
		 * [-2,1,-3,4,-1,2,1,-5,4] [1,2,3,4,5,6] [-2 -3 -5 -8 -1 -6] [] [-1] [1]
		 */
		int[] nums = new int[] {-2,1,-3,4,-1,2,1,-5,4};
		System.out.println(MaximumSubArray_LeetCode.maxSubArrayBruteForce(nums));
		//		System.out.println(MaximumSubArray_LeetCode.maxSubArray(nums));
		nums=null;
		System.out.println(MaximumSubArray_LeetCode.maxSubArrayBruteForce(nums));
		//		System.out.println(MaximumSubArray_LeetCode.maxSubArray(nums));
		nums=new int[] {1,2,3,4,5,6};
		System.out.println(MaximumSubArray_LeetCode.maxSubArrayBruteForce(nums));
		//		System.out.println(MaximumSubArray_LeetCode.maxSubArray(nums));
		nums=new int[] {-2,-3,-5,-8,-1,-6};
		System.out.println(MaximumSubArray_LeetCode.maxSubArrayBruteForce(nums));
		//		System.out.println(MaximumSubArray_LeetCode.maxSubArray(nums));
		nums=new int[] {};
		System.out.println(MaximumSubArray_LeetCode.maxSubArrayBruteForce(nums));
		//		System.out.println(MaximumSubArray_LeetCode.maxSubArray(nums));

	}

}



