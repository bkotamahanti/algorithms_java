package Arrays;

/**
 * Given an array and target find the index of element
 * 
 * @author bkotamahanti input: array, target output: index
 *
 */

public class BinarySearchSortedArray {
	// Time: O(logN)
	// Space:O(logN) call stack bcaz of recursion
	private static int binarySearchRecursion(int[] nums, int start, int end, int target) {
		// consider end cases when you reach 0 index or last index. otherwise it goes on
		// stack overflow
		if (start > end) {
			return -1;
		}

		// 1,2,3,4,5,6,7
		int mid = (start + end) / 2;
		if (nums[mid] == target) {
			return mid;
		} else if (target < nums[mid]) {
			return binarySearchRecursion(nums, start, mid - 1, target);
		} else {
			return binarySearchRecursion(nums, mid + 1, end, target);
		}

	}

	public static int mainBinarySearchRecursion(int[] nums, int target) {
		if (nums == null || nums.length == 0)
			return -1;
		int start = 0;
		int end = nums.length - 1;
		if (start == end)
			return (target == nums[start]) ? start : -1;
		return binarySearchRecursion(nums, start, end, target);

	}

	// Time:O(logN)
	// Space:O(1) since there is no call stack. Iterative is good for binary search.
	public static int binarySearchItr(int[] nums, int target) {
		if (nums == null || nums.length == 0)
			return -1;
		int start = 0;
		int end = nums.length - 1;
		if (start == end) {
			return (target == nums[start]) ? start : -1;
		}
		while (start <= end) {
			int mid = (start + end) / 2;
			if (nums[mid] == target)
				return mid;
			else if (target < nums[mid])
				end = mid - 1;
			else
				start = mid + 1;
		}
		return -1;
	}

	public static int binarySearchItr(int[] nums, int start, int end, int target) {
		if (nums == null || nums.length == 0)
			return -1;

		if (start == end) {
			return (target == nums[start]) ? start : -1;
		}
		while (start <= end) {
			int mid = (start + end) / 2;
			if (nums[mid] == target)
				return mid;
			else if (target < nums[mid])
				end = mid - 1;
			else
				start = mid + 1;
		}
		return -1;
	}

	public static void main(String[] args) {
		int[] nums = { 1, 2, 3, 4, 5, 6, 7 };
		System.out.println(BinarySearchSortedArray.binarySearchItr(nums, 6));
		System.out.println(BinarySearchSortedArray.mainBinarySearchRecursion(null, 5));
	}

}
