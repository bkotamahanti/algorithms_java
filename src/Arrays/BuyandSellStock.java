package Arrays;

public class BuyandSellStock {
	/*
    [7,1,5,3,6,4]
    [7,6,4,3,1]
    [7,5,10,1,3,4]
    []
    [7]

	 */
	public int maxProfit(int[] prices) {
		int max_profit=0;
        int min_price=Integer.MAX_VALUE;
        for(int i=0;i<prices.length;i++){
            if (prices[i] < min_price) {
                min_price=prices[i];
            }else if( (prices[i]-min_price) > max_profit) {
                max_profit= prices[i]-min_price;
                
            }
        }
        return max_profit;
		
	}
	
	public static void main(String[] s) {
		BuyandSellStock stock = new BuyandSellStock();
//		int[] prices = new int[]{7,1,5,3,6,4};
		int[] prices = new int[]{7,5,11,1,3,4};
		System.out.println( stock.maxProfit(prices)); 
	}

}
