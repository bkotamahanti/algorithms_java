package Arrays.Sorting;

import java.util.Arrays;

/**
 * 
 * @author bkotamahanti input: array, type: int output: array
 * 
 *         Using MergeSort Algorithm Time: O(NlognN) Space: O(N)
 *
 */

public class MergeSort {

	public int[] sortArray(int[] nums) {
		if (nums == null)
			return nums;
		int start = 0;
		int end = nums.length - 1;
		mergeSort(nums, start, end);
		// System.out.println(Arrays.toString(nums));
		// System.out.println(Arrays.asList(nums));
		return nums;

	}

	private void mergeSort(int[] nums, int start, int end) {
		if (start >= end)
			return;
		int mid = (start + end) / 2;
		mergeSort(nums, start, mid);
		mergeSort(nums, mid + 1, end);
		merge(nums, start, mid, end);

	}

	// Not creating any temp array instead creating 2 auxilary arrays of size
	// start--mid as L and mid+1 to end as R
	// and using main array to recopy elements. In that way you dont need to recopy
	// temp array to original array at the end of merge call.
	private void merge(int[] nums, int start, int mid, int end) {
		int n1 = mid - start + 1;
		int n2 = end - mid;
		int[] L = Arrays.copyOfRange(nums, start, mid + 1);
		int[] R = Arrays.copyOfRange(nums, mid + 1, end + 1);
		int i = 0, j = 0;
		int k = start;
		while (i < n1 && j < n2) {
			if (L[i] <= R[j]) {
				nums[k] = L[i];
				i++;
				k++;
			} else {
				nums[k] = R[j];
				j++;
				k++;
			}
		}
		while (i < n1) {
			nums[k] = L[i];
			k++;
			i++;
		}
		while (j < n2) {
			nums[k] = R[j];
			j++;
			k++;
		}
	}

	public static void main(String[] args) {
		MergeSort sort = new MergeSort();
		int[] arr = new int[] { 5, 2, 3, 1 };
		System.out.println(Arrays.toString(sort.sortArray(arr)));
		arr = null;
		System.out.println(Arrays.toString(sort.sortArray(arr)));
		arr = new int[] { 1 };
		System.out.println(Arrays.toString(sort.sortArray(arr)));
		arr = new int[] { 5, 1, 1, 2, 0, 0 };
		System.out.println(Arrays.toString(sort.sortArray(arr)));

	}

}
